// import rootReducer from 'react-redux'
// export const store = 
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// import root reducer
import { rootReducer } from './reducers/index';
const middlewares = [thunk]; // todo multiple middleware can be added

const initialState = {
    product: {
        products: [],
        isLoading: false,
        pageNumber: 1
    },
    user: {},
}

export const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares))


