import httpClient from "../../utils/httpClient";
import notification from "../../utils/notification";
export const ProductActionsType = {
    FETCH_PRODUCT_DATA: 'FETCH_PRODUCT_DATA',
    SET_IS_LOADING: 'SET_IS_LOADING',
    SET_PRODUCT_DATA: 'SET_PRODUCT_DATA',
    SET_PAGE_NUMBER: 'SET_PAGE_NUMBER',
}

// dispatch event to reducer


// export function fetch_product_ac(params){
//     return function(dispatch){

//     }
// }
export const fetch_product_ac = (params) => (dispatch) => {
    console.log('at action');
    dispatch({
        type: ProductActionsType.SET_IS_LOADING,
        payload: true
    })
    httpClient.GET('/product', true, params)
        .then(response => {
            console.log('http result');
            dispatch({
                type: ProductActionsType.FETCH_PRODUCT_DATA,
                payload: response.data
            })
        })
        .catch(err => {
            notification.handleError(err);
        })
        .finally(() => {
            dispatch({
                type: ProductActionsType.SET_IS_LOADING,
                payload: false
            })
        })
}

export const update_proudcts_data = (data) => (dispatch) => {
    console.log(' UI trigger at action')
    console.log('action dispatch data to reducer')
    dispatch({
        type: ProductActionsType.SET_PRODUCT_DATA,
        payload: data
    })
}

export const set_page_number_ac = (pageNumber) => (dispatch) => {
    console.log(' UI trigger at action')
    console.log('action dispatch data to reducer')
    dispatch({
        type: ProductActionsType.SET_PAGE_NUMBER,
        payload: pageNumber
    })
}