import axios from 'axios';
import { fireEvent } from '@testing-library/react';

const base_URL = process.env.REACT_APP_BASE_URL;
// create base instance
const http = axios.create({
    baseURL: base_URL,
    responseType: "json"
});
function getHeaders(isSecure = false) {
    let options = {
        'Content-Type': 'application/json'
    }
    if (isSecure) {
        options['Authorization'] = localStorage.getItem('token');
    }
    return options;
}

function GET(url, isSecure, params) {
    return http
        .get(url, {
            headers: getHeaders(isSecure),
            params: {
                ...params
            }
        })

}

function POST(url, data, isSecure) {
    return http
        .post(url, data, {
            headers: getHeaders(isSecure)
        });
}
function PUT(url, data, isSecure) {
    return http
        .put(url, data, {
            headers: getHeaders(isSecure)
        });
}
function DELETE(url, isSecure) {
    return http
        .delete(url, {
            headers: getHeaders(isSecure)
        });
}

function UPLOAD(method, url, data, files) {
    return new Promise((resolve, reject) => {
        // xml http request
        const xhr = new XMLHttpRequest();
        const formData = new FormData();

        // add image in from Data
        if (files && files.length) {
            formData.append('img', files[0], files[0].name)
        }
        // add text data in fromdata
        for (let key in data) {
            formData.append(key, data[key]);
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.response)
                }
            }
        }

        xhr.open(method, url, true);
        xhr.send(formData)
    })

}

export default {
    GET,
    POST,
    DELETE,
    PUT,
    UPLOAD
}