import React from 'react';
import './header.component.css';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

const NavBar = (props) => {

    const currentUser = JSON.parse(localStorage.getItem('user'))
    const logout = () => {
        // should logout and return to login page
        localStorage.clear();
        // navigate to login page
        props.history.push('/');
    }
    let nav = props.isLoggedIn ?
        <ul className="nav_list">
            <li className="nav_item">
                <Link to="/dashboard">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/profile">Profile</Link>

            </li>
            <li className="nav_item">
                <button className="btn btn-success logout" onClick={logout}>logout</button>
            </li>

        </ul>
        :
        <ul className="nav_list">
            <li className="nav_item">
                <Link to="/">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/">Login</Link>

            </li>
            <li className="nav_item">
                <Link to="/register">Register</Link>

            </li>
        </ul>

    return (
        nav
    )
};

export const Header = withRouter(NavBar);