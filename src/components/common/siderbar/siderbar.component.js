import React from 'react';
import './sidebar.component.css';
import { Link } from 'react-router-dom';

export const Sidebar = () => {
    return (<div className="sidenav">
        <Link to="/dashboard">Home</Link>
        <Link to="/add-product">Add Product</Link>
        <Link to="/view-product">View Product</Link>
        <Link to="/search-product">Search Product</Link>
        <Link to="/chat">Let's Chat</Link>
        <Link to="/profile">Profile</Link>
        <Link to="/settings">Settings</Link>
        <Link to="/change-password">Change Password</Link>
    </div>
    )
}