import React, { Component } from 'react'
import * as io from 'socket.io-client';
import './chat.component.css';
import dateProcessing from './../../../utils/dateProcessing';
import notify from './../../../utils/notification';

const defaultMessageContent = {
    sender: '',
    receiver: '',
    senderId: '',
    receiverId: '',
    message: '',
    time: ''
}
const socketURL = process.env.REACT_APP_SOCKET_URL;
export default class ChatComponent extends Component {
    constructor() {
        super();
        this.state = {
            messageBody: {
                ...defaultMessageContent
            },
            messages: [],
            currentUser: {},
            users: []
        }
    }

    componentDidMount() {
        const currentUser = JSON.parse(localStorage.getItem('user'));
        this.setState(() => ({
            currentUser,
        }), () => {
            this.socket.emit('new-user', this.state.currentUser.username);
        })
        this.socket = io(socketURL);
        this.runSocket();

    }

    runSocket() {
        this.socket.on('reply-msg-own', (data) => {
            const { messages } = this.state;
            messages.push(data);
            this.setState({
                messages,
            })
        })
        this.socket.on('reply-msg', (data) => {
            const { messages, messageBody } = this.state;
            messageBody.receiverId = data.senderId;
            messages.push(data);
            this.setState({
                messages,
                messageBody
            })
        })
        this.socket.on('users', (data) => {
            this.setState({
                users: data
            });
        })

    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(pre => ({
            messageBody: {
                ...pre.messageBody,
                [name]: value
            }
        }), () => {
            // console.log('messge in state >>', this.state);
        })
    }
    selectUser = user => {
        this.setState(pre => ({
            messageBody: {
                ...pre.messageBody,
                receiver: user.name,
                receiverId: user.id
            }
        }))
    }

    handleSubmit = e => {
        e.preventDefault();
        if (!this.state.messageBody.receiverId) {
            return notify.showInfo('Please Select a user to continue');
        }
        const { messageBody } = this.state;
        messageBody.sender = this.state.currentUser.username;
        messageBody.senderId = this.state.users.find((item, i) => item.name === this.state.currentUser.username).id;
        messageBody.time = new Date();
        this.socket.emit('new-msg', messageBody);
        this.setState((pre) => ({
            messageBody: {
                ...pre.messageBody,
                message: ''
            }
        }))
    }

    render() {
        return (
            <>
                <h2>Let's Chat</h2>
                <p><strong>{this.state.currentUser.username}</strong></p>
                <div className="row">
                    <div className="col-md-6">
                        <ins>Messages</ins>
                        <div className="message_block">
                            {this.state.messages.map((item, index) => (
                                <li className="message_list" key={index}>
                                    <p><strong>{item.sender}</strong></p>
                                    <p>{item.message}</p>
                                    <p>{dateProcessing.formatTime(item.time)}</p>
                                </li>
                            ))}
                        </div>
                        <form onSubmit={this.handleSubmit} className="form-group">
                            <input className="form-control" type="text" placeholder="Type your message here..." name="message" value={this.state.messageBody.message} onChange={this.handleChange}></input>
                            <button type="submit" className="btn btn-success">send</button>
                        </form>
                    </div>
                    <div className="col-md-6">
                        <ins>Users</ins>
                        <div className="message_block">
                            {this.state.users.map((item, index) => (
                                <li className="message_list" key={index}>
                                    <button className="btn btn-default" onClick={() => this.selectUser(item)}>{item.name}</button>
                                </li>
                            ))}
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
