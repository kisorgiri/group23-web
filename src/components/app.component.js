import React from 'react';
import { Routing } from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import { store } from './../store';


export const App = function (args) {
    // props 
    // props are the attribute set on components and props must be kept as placeholder in functional component
    // incoming data inside component(as a form of attributes) are props
    return (
        <div>
            <Provider store={store}>
                <Routing />
            </Provider>
            <ToastContainer />
        </div>
    )
}

// summarise
// library react-router-dom
// browser router >> configuration block <Router> vitra sabai config load hunu paryo
// route >> navigation config >> attributes path componet exact 
// route vitra component's value will have three props supplied from react-router-dom >> match,history,push
// switch >> it restricts one route is loaded at a time
// Link is similar to a tag used to redirect from template
// single page application is possible only because of react router dom


