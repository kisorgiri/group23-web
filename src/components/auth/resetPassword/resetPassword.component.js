import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import notification from '../../../utils/notification';
const url = 'http://localhost:8080/api/auth/reset-password'
export class ResetPasswordComponent extends React.Component {

    constructor() {
        super();
        this.state = {
            password: ''
        };
    }
    componentDidMount() {
        this.token = this.props.match.params.id;
    }

    handleChange = (e) => {
        console.log('e.target >>', e.target);
        this.setState({
            password: e.target.value
        })
    }
    handleSubmit = e => {
        e.preventDefault();
        console.log('my data is >>', this.state);
        axios.post(url + '/' + this.token, this.state)
            .then(data => {
                notification.showInfo('password reset successfull');
                this.props.history.push('/');
            })
            .catch(err => {
                notification.handleError(err);
            })
        // http call
    }

    render() {
        return (
            <div>
                <h2>Reset Password</h2>
                <p>Pleae choose your new password</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input className="form-control" type="password" placeholder="Password" name="password" onChange={this.handleChange}></input>
                    <br />
                    <button type="submit" className="btn btn-primary">submit</button>
                </form>
                <p>back to <Link to="/">login</Link></p>
            </div>
        )
    }
}