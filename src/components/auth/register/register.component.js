import React from 'react';
import { Link } from 'react-router-dom';
import notify from '../../../utils/notification';
import HttpClient from '../../../utils/httpClient';
const defaultForm = {
    name: '',
    email: '',
    username: '',
    password: '',
    gender: '',
    dob: '',
    address: '',
    phoneNumber: ''
}
export class Register extends React.Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm,
            },
            isValidForm: false,
            isSubmitting: false
        };
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'Username is required';
                break;

            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@')
                        ? ''
                        : 'Not an valid Email'
                    : 'Email is required';
                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'Weak Password'
                    : 'Password is required'
                break;
            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            // check is the form valid or not
            const { error } = this.state;
            let errors = Object
                .values(error)
                .filter(item => item);
            // .filter(function (item) {
            //     if (item) {
            //         return true;
            //     }
            // })
            this.setState({
                isValidForm: errors.length === 0
            })

        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({ isSubmitting: true });
        HttpClient
            .POST('/auth/register', this.state.data)
            .then((data) => {
                this.props.history.push('/');
                notify.showSuccess("Registration successfull please login!");
            })
            .catch((err) => {
                // send any error to error handling method
                notify.handleError(err);
            })
            .finally(() => {
                this.setState({ isSubmitting: false });
            })

    }

    // init update and destroy
    componentDidMount() {
        // it is a life cycle stage which will be executed once component fully loaded
        // console.log('props >>', this.props);
    }

    componentDidUpdate() {
        //     console.log('prev props >>', prevProps);
        //     console.log('prev state >>', prevState);
    }

    // destory
    componentWillUnmount() {
        // life cycle stage which will run when the component is destroyed
        // console.log('component is destroyed');
    }

    render() {

        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">Submitting...</button>
            : <button disabled={!this.state.isValidForm} className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>

        return (
            <div>
                <h2>Register</h2>
                <p>Please register to start using application</p>
                <form className="form-group" noValidate>
                    <label>Name</label>
                    <input className="form-control" onChange={this.handleChange} type="text" placeholder="Name" name="name"></input>
                    <label>Email</label>
                    <input className="form-control" onChange={this.handleChange} type="email" placeholder="Email" name="email"></input>
                    <p className="danger">{this.state.error.email}</p>
                    <label>Address</label>
                    <input className="form-control" onChange={this.handleChange} type="text" placeholder="Address" name="address"></input>
                    <label>Username</label>
                    <input className="form-control" onChange={this.handleChange} type="text" placeholder="Username" name="username"></input>
                    <p className="danger">{this.state.error.username}</p>
                    <label>Password</label>
                    <input className="form-control" onChange={this.handleChange} type="password" placeholder="Password" name="password"></input>
                    <p className="danger">{this.state.error.password}</p>
                    <label>phoneNumber</label>
                    <input className="form-control" onChange={this.handleChange} type="number" name="phoneNumber"></input>
                    <label>Gender</label>
                    <input className="form-control" onChange={this.handleChange} type="text" placeholder="Gender" name="gender"></input>
                    <label>D.O.B</label>
                    <input className="form-control" onChange={this.handleChange} type="date" name="dob"></input>
                    <br />
                    {btn}
                </form>
                <p>Current chapter : {this.state.chapter}</p>
                <p>Already registered?
                    <Link to="/">back to login</Link>
                </p>
            </div>
        )
    }
}
