import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import notify from '../../../utils/notification';
import httpClient from '../../../utils/httpClient';
const defaultForm = {
    username: '',
    password: ''
}
export class Login extends Component {

    constructor() {
        super();
        // to create a state declare a state object
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isFormValid: false,
            isSubmitting: false,
            remember_me: false
        };
        // component's data(internal) are preserved on state
        // usually class based component are declared to use states
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if (localStorage.getItem('remember_me')) {
            this.props.history.push('/dashboard');
        }
    }

    handleChange(ev) {
        // destruct
        const { name, value } = ev.target;
        this.setState(preState => (
            {
                data: {
                    ...preState.data,
                    [name]: value
                }
            }
        ), () => {
            this.validateForm(name)
        })


        // observe async flow
    }
    rememberMe(ev) {
        const { checked } = ev.target;
        // localstoreage
        localStorage.setItem('remember_me', checked);
    }

    validateForm(fieldName) {
        let errMsg;
        if (!this.state.data[fieldName]) {
            errMsg = `${fieldName} is required`
        }
        this.setState(prevState => ({
            error: {
                ...prevState.error,
                [fieldName]: errMsg
            }
        }), () => {
            // isValidForm TODO
            // to disbale the button
        })
    }
    handleSubmit(e) {
        e.preventDefault(); // prevent default behaviour
        this.setState({
            isSubmitting: true
        });
        httpClient
            .POST('/auth/login', this.state.data)
            .then((response) => {
                console.log('data is >>', response);
                notify.showSuccess(`Welcome ${response.data.user.username}`);

                // localstorage setup
                localStorage.setItem('user', JSON.stringify(response.data.user));
                localStorage.setItem('token', response.data.token);
                this.props.history.push('/dashboard');
            })
            .catch((err) => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            });
    }


    render() {
        // render method is must for class  based component
        // it must return single html node
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">logginin...</button>
            : <button onClick={this.handleSubmit} className="btn btn-primary">Login</button>

        return (
            <div>
                <h2>Login</h2>
                <p>Please Login to start your session</p>
                <form className="form-group" name="loginForm">
                    <label htmlFor="username">Email/Username</label>
                    <input className="form-control" type="text" placeholder="Email/Username" name="username" id="username" onChange={this.handleChange.bind(this)}></input>
                    <p>{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" name="password" placeholder="Password" onChange={this.handleChange.bind(this)} id="password"></input>
                    <p>{this.state.error.password}</p>
                    <input type="checkbox" name="remember_me" onChange={this.rememberMe.bind(this)} />
                    <label>Remember Me</label>
                    <br></br>
                    {btn}
                </form>
                <p className="float_left">Don't have an account? register <span><Link to="/register">here </Link> </span></p>
                <p className="float_right"><Link to="/forgot-password" >forgot password?</Link></p>
            </div>
        )
    }
}