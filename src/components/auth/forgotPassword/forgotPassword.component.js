import React, { Component } from 'react';
import httpClient from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import { Link } from 'react-router-dom';

export class ForgotPassword extends Component {


    constructor() {
        super();
        this.state = {
            isValidForm: false,
            isSubmitting: false,
            data: {
                email: ''
            },
            error: {
                email: ''
            }
        };
    }


    handleChange = (ev) => {
        // destruct
        const { name, value } = ev.target;
        this.setState(preState => (
            {
                data: {
                    ...preState.data,
                    [name]: value
                }
            }
        ), () => {
            this.validateForm(name)
        })
    }


    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@')
                        ? ''
                        : 'Not an valid Email'
                    : 'Email is required';
                break;
            default:
                break;
        }
        this.setState(prevState => ({
            error: {
                ...prevState.error,
                [fieldName]: errMsg
            }
        }), () => {
            let errors = Object.values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            });
        })
    }
    handleSubmit = (e) => {
        e.preventDefault(); // prevent default behaviour
        this.setState({
            isSubmitting: true
        });
        httpClient
            .POST('/auth/forgot-password', this.state.data)
            .then((response) => {
                notification.showSuccess('Password reset link sent to your email please check your inbox');

                this.props.history.push('/');
            })
            .catch((err) => {
                notification.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            });
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} type="submit" className="btn btn-primary">submit</button>
        return (
            <div>
                <h2>Forgot Password</h2>
                <p>Please Provide your email address to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="email">Email</label>
                    <input className="form-control" type="text" placeholder="Email" name="email" id="email" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.email}</p>

                    <br></br>
                    {btn}
                </form>
                <p className="float_left">Back to <span><Link to="/">login </Link> </span></p>
            </div>
        )
    }
}