import React, { Component } from 'react'
import httpClient from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import { ViewProduct } from './../viewProducts/viewProduct.component';

const defaultForm = {
    name: '',
    category: '',
    minPrice: '',
    maxPrice: '',
    color: '',
    fromDate: '',
    toDate: '',
    discountedItem: '',
    multipleDateRange: '',
    tags: ''
}
export default class SearchProduct extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            allProducts: [],
            categories: [],
            names: [],
            isSubmitting: false,
            isValidForm: false,
            searchResult: []
        };
    }

    componentDidMount() {
        httpClient.POST('/product/search', {})
            .then(response => {
                const categories = [];
                response.data.forEach((item, index) => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category);
                    }
                });
                this.setState({
                    allProducts: response.data,
                    categories
                })

            })
            .catch(err => notification.handleError(err));
    }

    handleChange = (e) => {
        let { type, name, value, checked } = e.target;
        if (type === 'checkbox') {
            value = checked
        }
        if (name === 'category') {
            this.buildNameOptions(value);
        }

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    buildNameOptions(selectedCategory) {
        const names = this.state.allProducts.filter(item => item.category === selectedCategory);
        this.setState({
            names
        });
    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'category':
                errMsg = this.state.data[fieldName] ? '' : 'required field*';
                break;
            default:
                break;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }),
            () => {
                let errors = Object.values(this.state.error)
                    .filter(err => err);
                this.setState({
                    isValidForm: errors.length === 0
                });
            })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        const { data } = this.state;
        if (!data.multipleDateRange) {
            data.toDate = null;
        }
        if (!data.toDate) {
            data.toDate = data.fromDate
        }
        httpClient.GET('/product/search',false, data)
            .then(response => {
                if (!response.data.length) {
                    return notification.showInfo("No Any Product matched your search query");
                }
                this.setState({
                    searchResult: response.data
                });
            })
            .catch(err => notification.handleError(err))
            .finally(() => this.setState({ isSubmitting: false }))

    }

    searchAgain = (e) => {
        e.preventDefault();
        this.setState({
            data: {
                ...defaultForm
            },
            searchResult: []
        });
    }
    render() {

        let toDateContent = this.state.data.multipleDateRange
            ? <>
                <label>To Date</label>
                <input value={this.state.data.toDate} className="form-control" type="date" name="toDate" onChange={this.handleChange}></input>
            </>
            : ''
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} type="submit" className="btn btn-primary">submit</button>
        let categoriesOptions = this.state.categories.map((item, index) => (
            <option key={index} value={item}>{item}</option>
        ));
        let nameOptions = this.state.names.map((item, index) => (
            <option key={index} value={item.name}>{item.name}</option>
        ));
        let nameContent = this.state.data.category
            ?
            <>
                <label>Name</label>
                <select name="name" onChange={this.handleChange} className="form-control">
                    <option value="">(Select Name)</option>
                    {nameOptions}
                </select>
            </>
            : '';
        let searchContent = <>
            <h2>{this.props.title}</h2>
            <form className="form-group" onSubmit={this.handleSubmit}>


                <label>Category</label>
                <select name="category" onChange={this.handleChange} className="form-control">
                    <option value="">(Select Category)</option>
                    {categoriesOptions}
                </select>
                <p className="danger">{this.state.error.category}</p>
                {nameContent}
                <label>Min Price</label>
                <input className="form-control" type="number" placeholder="Min Price" name="minPrice" onChange={this.handleChange}></input>
                <label>Max Price</label>
                <input className="form-control" type="number" placeholder="Max Price" name="maxPrice" onChange={this.handleChange}></input>
                <label>Color</label>
                <input value={this.state.data.color} className="form-control" type="text" placeholder="Color" name="color" onChange={this.handleChange}></input>
                <label>Select Date</label>
                <input value={this.state.data.fromDate} className="form-control" type="date" name="fromDate" onChange={this.handleChange}></input>
                <br />
                <input type="checkbox" name="multipleDateRange" onChange={this.handleChange} ></input>
                <label>Multiple Date Range</label>
                <br />
                {toDateContent}
                <label>Tags</label>
                <input value={this.state.data.tags} className="form-control" type="text" placeholder="Tags" name="tags" onChange={this.handleChange}></input>
                <input checked={this.state.data.discountedItem} type="checkbox" name="discountedItem" onChange={this.handleChange}></input>
                <label>Discounted Item</label>
                <br />
                {btn}
            </form>
        </>

        let mainDiv = this.state.searchResult.length
            ? <ViewProduct incomingData={this.state.searchResult} action={this.searchAgain} ></ViewProduct>
            : searchContent
        return (
            mainDiv
        )
    }
}
