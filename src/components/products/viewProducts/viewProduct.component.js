import React, { Component } from 'react';
import httpClient from '../../../utils/httpClient';
import notify from '../../../utils/notification';
import { Loader } from '../../common/loader/loader.componet';
import { Link } from 'react-router-dom';
import processDate from './../../../utils/dateProcessing';
import { connect } from 'react-redux';
import { fetch_product_ac, update_proudcts_data, set_page_number_ac } from '../../../actions/products/productActions';

const ImgUrl = process.env.REACT_APP_IMG_URL;
class ViewProductComponent extends Component {

    constructor() {
        super();
        this.state = {};
    }
    componentDidMount() {
        console.log('props >>', this.props);
        if (this.props.incomingData) {
            this.props.load_data_to_store(this.props.incomingData);
        } else {

            this.props.fetch(1, 5);
        }

    }
    // componentWillReceiveProps() {
    //     console.log('this.props >', this.props);
    // }
    removeProduct(id, index) {
        // confirmation 
        // eslint-disable-next-line no-restricted-globals
        let confirmation = confirm('Are you sure to Remove?');
        if (confirmation) {
            httpClient.DELETE(`/product/${id}`, true)
                .then(data => {
                    notify.showInfo('Product deleted');
                    const { products } = this.state;
                    products.splice(index, 1);
                    this.setState({ products });
                })
                .catch(err => notify.handleError(err));
        }
    }

    set_page_num(action) {
        let { pageNumber } = this.props;
        let currentPage = pageNumber || 1;
        if (action === 'next') {
            currentPage += 1;
        }
        if (action === 'previous') {
            currentPage -= 1;
        }
        console.log('current page NUmber >>', currentPage)
        this.props.fetch(currentPage,5); // http call
        this.props.change_page_num(currentPage); // change pagenumber in store

    }

    render() {
        let searchAgainContent = this.props.incomingData
            ? <button className="btn btn-success" onClick={this.props.action}>Search Again</button>
            : '';
        let tableContent = this.props.products.map((item, index) => (
            <tr key={item._id}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.category}</td>
                <td>{item.price}</td>
                <td>{processDate.formatDate(item.createdAt)}</td>
                {/* <td>{processDate.formatTime(item.createdAt)}</td> */}
                <td>
                    <img src={`${ImgUrl}/${item.images[0]}`} alt="productImage.jpg" width="200"></img>
                </td>
                <td>
                    <Link className="btn btn-info" to={`/edit-product/${item._id}`}>edit</Link>
                    <button onClick={() => this.removeProduct(item._id, index)} className="btn btn-danger">delete</button>
                </td>
            </tr>
        ));
        let mainContent = this.props.isLoading
            ? <Loader />
            : <>
                <table className="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Created Date</th>
                            {/* <th>Created Time</th> */}
                            <th>Images</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableContent}
                    </tbody>
                </table>
                <button className="btn btn-success" onClick={() => this.set_page_num('previous')}>previous</button>
                <button className="btn btn-success" onClick={() => this.set_page_num('next')}>next</button>
            </>

        return (
            <>
                <h2>View Products</h2>
                {searchAgainContent}
                {mainContent}

            </>
        )
    }
}
// props
const mapStateToProps = store => ({
    products: store.product.products,
    isLoading: store.product.isLoading,
    pageNumber: store.product.pageNumber
});
// incoming value for componet as props

const mapDispatchToProps = dispatch => ({
    fetch: (pageNumber, pageSize) => dispatch(fetch_product_ac({ pageNumber, pageSize })),
    load_data_to_store: () => dispatch(update_proudcts_data),
    change_page_num: (pageNumber) => dispatch(set_page_number_ac(pageNumber))
});
// click event(outgoing value from our component as porps)

export const ViewProduct = connect(mapStateToProps, mapDispatchToProps)(ViewProductComponent);