import React, { Component } from 'react';
import httpClient from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import { Loader } from '../../common/loader/loader.componet';
import { ProductForm } from '../productForm/productForm.component';

export class EditProduct extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            product: {}
        }
    }

    componentDidMount() {
        let productId = this.props.match.params.id;
        this.setState({
            isLoading: true
        })
        httpClient.GET(`/product/${productId}`, true)
            .then(response => {
                console.log('response is >>', response);
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            });
    }

    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <ProductForm title="Edit Product" data={this.state.product}></ProductForm>
        return content;
    }
}