import React, { Component } from 'react';
import httpClient from '../../../utils/httpClient';
import notify from '../../../utils/notification';
import { withRouter } from 'react-router-dom';
const ImgUrl = process.env.REACT_APP_IMG_URL;
const BaseURL = process.env.REACT_APP_BASE_URL;
const defaultForm = {
    name: '',
    description: '',
    brand: '',
    category: '',
    price: '',
    color: '',
    weight: '',
    tags: '',
    image: '',
    manuDate: '',
    expiryDate: '',
    discountedItem: '',
    discountType: '',
    discountValue: '',
}
class ProductFormComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            filesToUpload: [],
            isSubmitting: false,
            isValidForm: false,
        };
    }
    componentDidMount() {
        if (this.props.data) { //edit
            this.setState({
                data: {
                    ...defaultForm,
                    ...this.props.data,
                    discountedItem: this.props.data.discount ? this.props.data.discount.discountedItem : '',
                    discountType: this.props.data.discount ? this.props.data.discount.discountType : '',
                    discountValue: this.props.data.discount ? this.props.data.discount.discountValue : '',
                    image: this.props.data.images ? this.props.data.images[0] : ''
                }
            })
        }
    }

    handleChange = (e) => {
        let { type, name, value, checked, files } = e.target;
        if (type === 'checkbox') {
            value = checked
        }
        if (type === 'file') {
            this.setState({
                filesToUpload: files
            });
            value = ''
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'category':
                errMsg = this.state.data[fieldName] ? '' : 'required field*';
                break;
            default:
                break;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }),
            () => {
                let errors = Object.values(this.state.error)
                    .filter(err => err);
                this.setState({
                    isValidForm: errors.length === 0
                });
            })
    }

    handleSubmit = (e) => {
        // this.props.onClick(); 
        this.setState({
            isSubmitting: false
        })
        e.preventDefault();
        console.log('this.state.filesToUpload', this.state.filesToUpload);
        if (this.state.data._id) {
            this.update(this.state.data._id);
        } else {
            this.add();
        }
    }

    add() {

        httpClient.UPLOAD("POST", `${BaseURL}/product?token=${localStorage.getItem('token')}`, this.state.data, this.state.filesToUpload)
            .then(response => {
                notify.showSuccess('Product Added Successfully');
                this.props.history.push('/view-product')
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            });
    }
    update(productId) {
        httpClient.UPLOAD("PUT",`${BaseURL}/product/${productId}?token=${localStorage.getItem('token')}`, this.state.data, this.state.filesToUpload)
            .then(response => {
                notify.showSuccess('Product Updated Successfully');
                this.props.history.push('/view-product')
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            });
    }
    render() {
        let showPreviousImage = this.state.data._id ?
            <>
                <label>Previous Image</label>
                <img src={`${ImgUrl}/${this.state.data.image}`} alt="productImage.jpg" width="600"></img>
                <br />
            </>
            : ''
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} type="submit" className="btn btn-primary">submit</button>

        let discountDetails = this.state.data.discountedItem
            ? <>
                <label>Discount Type</label>
                <input value={this.state.data.discountType} className="form-control" type="text" placeholder="Discount Type" name="discountType" onChange={this.handleChange}></input>
                <label>Discount Value</label>
                <input value={this.state.data.discountValue} className="form-control" type="text" placeholder="Discount Value" name="discountValue" onChange={this.handleChange}></input>
                <br />
            </>
            : ''
        return (
            <>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input value={this.state.data.name} className="form-control" type="text" placeholder="Name" name="name" onChange={this.handleChange}></input>
                    <label>Description</label>
                    <input value={this.state.data.description} className="form-control" type="text" placeholder="Description" name="description" onChange={this.handleChange}></input>
                    <label>Brand</label>
                    <input value={this.state.data.brand} className="form-control" type="text" placeholder="Brand" name="brand" onChange={this.handleChange}></input>
                    <label>Category</label>
                    <input value={this.state.data.category} className="form-control" type="text" placeholder="Category" name="category" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.category}</p>
                    <label>Price</label>
                    <input value={this.state.data.price} className="form-control" type="text" placeholder="Price" name="price" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input value={this.state.data.color} className="form-control" type="text" placeholder="Color" name="color" onChange={this.handleChange}></input>
                    <label>Weight</label>
                    <input value={this.state.data.weight} className="form-control" type="text" placeholder="Weight" name="weight" onChange={this.handleChange}></input>
                    <label>Manu. Date</label>
                    <input value={this.state.data.manuDate} className="form-control" type="date" name="manuDate" onChange={this.handleChange}></input>
                    <label>Expiry Date</label>
                    <input value={this.state.data.expiryDate} className="form-control" type="date" name="expiryDate" onChange={this.handleChange}></input>
                    <label>Tags</label>
                    <input value={this.state.data.tags} className="form-control" type="text" placeholder="Tags" name="tags" onChange={this.handleChange}></input>
                    <br />
                    {showPreviousImage}
                    <input type="file" onChange={this.handleChange} name="image"></input>
                    <br />
                    <input checked={this.state.data.discountedItem} type="checkbox" name="discountedItem" onChange={this.handleChange}></input>
                    <label>Discounted Item</label>
                    <br />
                    {discountDetails}
                    {btn}
                </form>
            </>
        )
    }
}

export const ProductForm = withRouter(ProductFormComponent);