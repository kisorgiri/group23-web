import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Header } from './common/header/header.component';
import { Sidebar } from './common/siderbar/siderbar.component';
import { ViewProduct } from './products/viewProducts/viewProduct.component';
import { EditProduct } from './products/editProduct/editProduct.component';
import { AddProduct } from './products/addProduct/addProduct.component';
import SearchProduct from './products/searchProduct/searchProduct';
import { Register } from './auth/register/register.component';
import { Login } from './auth/login/login.component';
import { ForgotPassword } from './auth/forgotPassword/forgotPassword.component';
import { ResetPasswordComponent } from './auth/resetPassword/resetPassword.component';
import ChatComponent from './common/chat/chat.component';



const PageNotFound = () => {
    return (
        <>
            <p>Page Not Found</p>
            <img src="/images/404.jpeg" alt="pagenotfound.jpg" width="600px"></img>
        </>
    )

}


const Dashboard = (props) => {
    console.log('props is >>', props);

    return <p>Welcome to Group 23 Web Please use side navigation menu or contact system administrator for help</p>
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        localStorage.getItem('token')
            ? <>
                <div className="nav_bar">
                    <Header isLoggedIn={true} />
                </div>
                <div className="sidenav">
                    <Sidebar></Sidebar>
                </div>
                <div className="main">
                    <Component {...routeProps}></Component>
                </div>
            </>
            : <Redirect to="/" ></Redirect>

    )}>

    </Route >
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        <>
            <div className="nav_bar">
                <Header isLoggedIn={localStorage.getItem('token') ? true : false} />
            </div>
            <div className="main">
                <Component {...routeProps}></Component>
            </div>
        </>

    )}>

    </Route >
}
export const Routing = () => {
    return (
        <Router>
            <Switch>
                <PublicRoute exact path="/" component={Login}></PublicRoute>
                <PublicRoute path="/register" component={Register}></PublicRoute>
                <PublicRoute path="/forgot-password" component={ForgotPassword}></PublicRoute>
                <PublicRoute path="/reset-password/:id" component={ResetPasswordComponent}></PublicRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard} ></ProtectedRoute>
                <ProtectedRoute path="/chat" component={ChatComponent} ></ProtectedRoute>
                <ProtectedRoute path="/add-product" component={AddProduct} ></ProtectedRoute>
                <ProtectedRoute path="/view-product" component={ViewProduct} ></ProtectedRoute>
                <ProtectedRoute path="/search-product" component={SearchProduct} ></ProtectedRoute>
                <ProtectedRoute path="/edit-product/:id" component={EditProduct} ></ProtectedRoute>
                <PublicRoute component={PageNotFound} ></PublicRoute>
            </Switch>
        </Router >
    )
}