
import { combineReducers } from 'redux';
import { productReducer } from './product_reducer';

export const rootReducer = combineReducers({
    product: productReducer
})