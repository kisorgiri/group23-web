

//pure function
// pure will always return same value
import { ProductActionsType } from '../actions/products/productActions';
const initialState = {
    products: [],
    isLoading: false,
    pageNumber: 1,
    pageSize: 5
}

export const productReducer = (state = initialState, action) => {
    console.log('action in reducers>>', action);
    console.log('reducer always return new state');

    switch (action.type) {
        case ProductActionsType.FETCH_PRODUCT_DATA:
            return {
                ...state,
                products: action.payload
            }

        case ProductActionsType.SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case ProductActionsType.SET_PRODUCT_DATA:
            return {
                ...state,
                products: action.payload
            }
        case ProductActionsType.SET_PAGE_NUMBER:
            return {
                ...state,
                pageNumber: action.payload
            }

        default:
            return {
                ...state
            }
    }
}